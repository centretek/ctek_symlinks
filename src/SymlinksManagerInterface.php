<?php

namespace Drupal\ctek_symlinks;

use Drupal\Core\CacheDecorator\CacheDecoratorInterface;
use Drupal\node\NodeInterface;
use Drupal\ctek_symlinks\Plugin\Menu\MenuLinkSymbolic;

interface SymlinksManagerInterface extends CacheDecoratorInterface {

  public function upsert($source, $symlink, MenuLinkSymbolic $link = NULL, $forceDefault = FALSE);

  public function deleteAllSymlinksBySource($source);

  public function updateSymlinksForSource($source, array $symlinks);

  public function getSymlinkByPath($path, $domain);

  public function getAllSymlinksBySource($source);

  public function getDefaultSymlinkBySource($source, $domain);

  public function getSymlinkFromMenuLink(MenuLinkSymbolic $link);

  public function getNodeMenuLinkForPath(NodeInterface $node, $path, $domain);

  public function getMenuLinkFromSymlink($symlink);

}
