<?php

namespace Drupal\ctek_symlinks\EventSubscriber;

use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\ctek_symlinks\SymlinksManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class SymlinksSubscriber implements EventSubscriberInterface {

  protected $symlinksManager;

  protected $currentPathStack;

  protected $routeProvider;

  public function __construct(
    SymlinksManagerInterface $symlinksManager,
    CurrentPathStack $currentPathStack,
    RouteProviderInterface $routeProvider
  ) {
    $this->symlinksManager = $symlinksManager;
    $this->currentPathStack = $currentPathStack;
    $this->routeProvider = $routeProvider;
  }

  public function onKernelController(ControllerEvent $event) {
    if ($event->getRequestType() === HttpKernelInterface::MASTER_REQUEST) {
      $this->symlinksManager->setCacheKey($event->getRequest()->getHost() . rtrim($this->currentPathStack->getPath($event->getRequest()), '/'));
    }
  }

  public function onKernelTerminate(TerminateEvent $event) {
    $this->symlinksManager->writeCache();
  }

  /**
   * Adds the active workspace as a cache key part to the route provider.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   An event object.
   */
  public function onKernelRequest(RequestEvent $event) {
    $this->routeProvider->addExtraCacheKeyPart('ctek_symlinks', $event->getRequest()->getHost());
  }

  public static function getSubscribedEvents() {
    return [
      KernelEvents::CONTROLLER => [
        ['onKernelController', 200],
      ],
      KernelEvents::TERMINATE => [
        ['onKernelTerminate', 200],
      ],
      KernelEvents::REQUEST => [
        'onKernelRequest', 33
      ],
    ];
  }

}
