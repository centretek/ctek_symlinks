<?php

namespace Drupal\ctek_symlinks\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\ctek_symlinks\SymlinksManagerInterface;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\HttpFoundation\Request;

class SymlinksPathProcessor implements InboundPathProcessorInterface, OutboundPathProcessorInterface {

  protected $symlinksManager;

  public function __construct(SymlinksManagerInterface $nodeSymlinksManager) {
    $this->symlinksManager = $nodeSymlinksManager;
  }

  public function processInbound($path, Request $request) {
    $domain = NULL;
    if (\Drupal::moduleHandler()->moduleExists('ctek_domain_context')) {
      $term = \Drupal::service('ctek_domain_context.helper')->getActiveDomainTerm();
      if ($term instanceof TermInterface) {
        $domain = $term->id();
      }
    }
    if ($symlink = $this->symlinksManager->getSymlinkByPath($path, $domain)) {
      $path = $symlink->source;
    }
    return $path;
  }

  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    if (!preg_match('|^/node/\d+$|', $path)) {
      return $path;
    }
    [,,$nid] = explode('/', $path);
    $domain = NULL;
    $bubbleable_metadata->addCacheTags([
      'node:' . $nid,
      'node_list',
      'symlinks:' . $path,
    ]);
    if ($bubbleable_metadata) {
      if (\Drupal::moduleHandler()->moduleExists('ctek_domain_context')) {
        $bubbleable_metadata->addCacheContexts(['url']);
        $term = \Drupal::service('ctek_domain_context.helper')->getActiveDomainTerm();
        if ($term instanceof TermInterface) {
          $domain = $term->id();
        }
      }
    }
    // Menu has injected a symlink already, use it.
    if (isset($options['symlink'])) {
      // Tell PathProcessorAlias to take a hike.
      $options['alias'] = TRUE;

      return $options['symlink']->symlink;
    }
    // See if there is a default symlink.
    if ($symlink = $this->symlinksManager->getDefaultSymlinkBySource($path, $domain)) {
      return $symlink->symlink;
    }
    return $path;
  }

}
