<?php

namespace Drupal\ctek_symlinks\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Ajax\AfterCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Menu\MenuParentFormSelectorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Drupal\ctek_symlinks\Entity\MenuLinkSymbolic;
use Drupal\ctek_symlinks\SymlinksManagerInterface;
use Drupal\system\Entity\Menu;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SymlinksForm extends ContentEntityForm {

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('ctek_symlinks.symlinks_manager'),
      $container->get('plugin.manager.menu.link'),
      $container->get('menu.parent_form_selector'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * @var \Drupal\node\NodeInterface
   */
  protected $entity;

  protected $entityTypeManager;

  protected $symlinksManager;

  protected $menuLinkManager;

  protected $menuParentFormSelector;

  protected $menus;

  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    SymlinksManagerInterface $symlinksManager,
    MenuLinkManagerInterface $menuLinkManager,
    MenuParentFormSelectorInterface $menuParentFormSelector,
    EntityRepositoryInterface $entityRepository,
    EntityTypeBundleInfoInterface $entityTypeBundleInfo = NULL,
    TimeInterface $time = NULL
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->symlinksManager = $symlinksManager;
    $this->menuLinkManager = $menuLinkManager;
    $this->menuParentFormSelector = $menuParentFormSelector;
    parent::__construct($entityRepository, $entityTypeBundleInfo, $time);
  }

  public function getBaseFormId() {
    return NULL;
  }

  public function getFormId() {
    return 'ctek_symlinks_symlinks_form';
  }

  protected function getMenus() {
    if ($this->menus === NULL) {
      $this->menus = $this->entityTypeManager->getStorage('menu')->loadByProperties();
    }
    $menus = $this->menus;
    $account = $this->currentUser();
    \Drupal::moduleHandler()->alter('nodesymlinks_available_menus', $menus, $this->entity, $account);
    return $menus;
  }

  protected function getSymlinksHeader() {
    return [
      [
        'data' => $this->t('Menu Link'),
      ],
      [
        'data' => $this->t('Path'),
      ],
      [
        'data' => $this->t('Is Default'),
      ],
      [
        'data' => $this->t('Actions'),
      ],
    ];
  }

  protected function buildSymlinkRows($symlinks, FormStateInterface $formState) {
    $rows = [];
    foreach ($symlinks as $index => $symlink) {
      $rows[$index] = $this->buildSymlinkRow($symlink, $index, $formState);
    }
    return $rows;
  }

  protected function buildSymlinkRow($symlink, $index, FormStateInterface $formState) {
    $row = [
      'menu_link' => $this->getMenuElement($symlink->link_id, $index, $formState, $symlink),
      'symlink' => [
        '#type' => 'container',
        'path' => [
          '#type' => 'textfield',
          '#default_value' => $symlink->symlink,
          '#title' => $this->t('Path'),
          '#required' => TRUE,
          '#element_validate' => ['::validatePath'],
        ],
        'id' => [
          '#type' => 'value',
          '#value' => $symlink->id,
        ],
      ],
      'is_default' => [
        '#markup' => $symlink->is_default ? $this->t('Yes') : $this->t('No'),
      ],
      'actions' => [
        '#type' => 'container',
        'make_default' => [
          '#type' => 'submit',
          '#submit' => ['::makeDefault'],
          '#ajax' => [
            'callback' => '::ajaxTableCallback',
            'wrapper' => 'ctek_symlinks_table',
          ],
          '#value' => $this->t('Make Default'),
          '#name' => 'make_default__' . $index,
          '#index' => $index,
        ],
        'remove' => [
          '#type' => 'submit',
          '#submit' => ['::removeSymlink'],
          '#ajax' => [
            'callback' => '::ajaxTableCallback',
            'wrapper' => 'ctek_symlinks_table',
          ],
          '#limit_validation_errors' => [],
          '#value' => $this->t('Remove'),
          '#name' => 'remove__' . $index,
          '#index' => $index,
        ],
      ],
    ];
    if (\Drupal::moduleHandler()->moduleExists('pathauto')) {
      /** @var \Drupal\pathauto\PathautoGenerator $pathautoGenerator */
      $pathautoGenerator = \Drupal::service('pathauto.generator');
      $pattern = $pathautoGenerator->getPatternByEntity($this->entity);
      if ($pattern) {
        $row['symlink']['path']['#prefix'] = '<div id="ctek_symlinks_pathauto_path_wrapper_' . $index . '">';
        $row['symlink']['path']['#suffix'] = '</div>';
        $row['symlink']['pathauto'] = [
          '#type' => 'submit',
          '#submit' => ['::generatePathauto'],
          '#ajax' => [
            'callback' => '::pathautoCallback',
          ],
          '#limit_validation_errors' => [],
          '#value' => $this->t('Generate using Pathauto'),
          '#name' => 'pathauto__' . $index,
          '#index' => $index,
        ];
      }
    }
    if (\Drupal::moduleHandler()->moduleExists('ctek_domain_context')) {
      $terms = \Drupal::service('ctek_domain_context.helper')->getDomainTerms();
      $row['symlink']['domain'] = [
        '#type' => 'radios',
        '#default_value' => $symlink->domain ?? 0,
        '#title' => $this->t('Domain'),
        '#required' => TRUE,
        '#options' => ['None'] + array_map(function(TermInterface $term){
          return $term->getName();
        }, $terms),
      ];
    }
    return $row;
  }

  public function validatePath(array $element, FormStateInterface $formState, array $form) {
    $symlink = $formState->getValue(array_slice($element['#parents'], 0, -1));
    $existing = $this->symlinksManager->getSymlinkByPath($symlink['path'], $symlink['domain'] ?? NULL);
    if ($existing && intval($existing->id) !== intval($symlink['id'])) {
      $formState->setError($element, $this->t('Cannot create multiple symlinks with the same path.'));
    }
    try {
      Url::fromUri('internal:' . $symlink['path']);
    } catch (\Exception $e) {
      $formState->setError($element, $this->t('Invalid path.'));
    }
  }

  protected function getMenuElement($linkId, $index, FormStateInterface $formState, $symlink) {
    $element = [
      '#type' => 'container',
      'menu' => [
        '#type' => 'select',
        '#title' => $this->t('Menu'),
        '#options' => array_map(function(Menu $menu){
          return $menu->label();
        }, $this->getMenus()),
        '#empty_option' => $this->t('- No Menu Link -'),
        '#empty_value' => NULL,
        '#ajax' => [
          'callback' => '::ajaxTableCallback',
          'wrapper' => 'ctek_symlinks_table',
        ],
      ],
      'is_default' => [
        '#type' => 'value',
        '#value' => $symlink->is_default,
      ],
    ];
    $element['existing_id'] = [
      '#type' => 'value',
      '#default_value' => $linkId,
    ];
    $linkEntity = $linkId ? MenuLinkSymbolic::load($linkId) : NULL;
    /** @var \Drupal\ctek_symlinks\Plugin\Menu\MenuLinkSymbolic $linkPlugin */
    $linkPlugin = $linkEntity ? $this->menuLinkManager->createInstance($linkEntity->getPluginId()) : NULL;
    $menuName = $formState->getValue(['symlinks', $index, 'menu_link', 'menu'], $linkPlugin ? $linkPlugin->getMenuName() : NULL);
    $element['menu']['#default_value'] = $linkPlugin ? $linkPlugin->getMenuName() : NULL;
    if ($menuName) {
      if (!isset($this->getMenus()[$menuName])) {
        $element['menu']['#access'] = FALSE;
        $element['access_denied'] = [
          '#markup' => '<div class="messages messages--warning">' . $this->t('You do not have access to edit the menu link for this Symlink.') . '</div>',
        ];
        $element['parent'] = [
          '#type' => 'value',
          '#value' => $menuName . ':' . ($linkPlugin ? $linkPlugin->getParent() : NULL),
        ];
      } else {
        $menu = $this->getMenus()[$menuName];
        $element['parent'] = $this->menuParentFormSelector->parentSelectElement(
            $menuName . ':' . ($linkPlugin ? $linkPlugin->getParent() : NULL),
            $linkPlugin ? $linkPlugin->getPluginId() : NULL,
            [$menuName => $menu->label()]
          ) + ['#title' => $this->t('Parent')];
      }
      $title = $formState->getValue(['symlinks', $index, 'menu_link', 'title'], $linkPlugin ? $linkPlugin->getTitle() : NULL);
      $element['title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Title'),
        '#default_value' => $title,
        '#required' => TRUE,
        '#access' => isset($this->getMenus()[$menuName]),
      ];
      if ($linkPlugin) {
        $element['id'] = [
          '#type' => 'value',
          '#value' => $linkPlugin->getEntityId(),
        ];
      }
    }
    return $element;
  }

  public function getSymlinks($symlinks, FormStateInterface $form_state) {
    $element = [
      '#type' => 'table',
      '#header' => $this->getSymlinksHeader(),
      '#empty' => $this->t('This node does not have any symlinks.'),
      '#prefix' => '<div id="ctek_symlinks_table">',
      '#suffix' => '</div>',
    ];
    $rows = $this->buildSymlinkRows($symlinks, $form_state);
    if (count($rows) > 0) {
      $element += $rows;
    } else {
      $element['#value'] = [];
    }

    return $element;
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\node\NodeTypeInterface $nodeType */
    $nodeType = \Drupal::entityTypeManager()->getStorage('node_type')->load($this->entity->bundle());
    if (!$nodeType->getThirdPartySetting('ctek_symlinks', 'enable')) {
      \Drupal::messenger()->addWarning($this->t('Symlinks have not been enabled for this content type.'));
      return;
    }
    $symlinks =& $form_state->getStorage()['symlinks'];
    $source = '/node/' . $this->entity->id();
    if ($symlinks === NULL) {
      $symlinks = $this->symlinksManager->getAllSymlinksBySource($source);
    }
    $form['symlinks'] = $this->getSymlinks($symlinks, $form_state);
    $form['symlinks_actions'] = [
      '#type' => 'actions',
      'add' => [
        '#type' => 'submit',
        '#value' => $this->t('Add Symlink'),
        '#submit' => ['::addSymlink'],
        '#ajax' => [
          'callback' => '::ajaxTableCallback',
          'wrapper' => 'ctek_symlinks_table',
        ],
      ],
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#name' => 'submit',
      ],
    ];
    return $form;
  }

  public function addSymlink(array $form, FormStateInterface $form_state) {
    $symlinks =& $form_state->getStorage()['symlinks'];
    $symlinks[] = (object)[
      'id' => NULL,
      'symlink' => NULL,
      'is_default' => count($symlinks) === 0,
      'link_id' => NULL,
    ];
    $form_state->setRebuild();
  }

  public function makeDefault(array $form, FormStateInterface $form_state) {
    $symlinks =& $form_state->getStorage()['symlinks'];
    $triggeringIndex = $form_state->getTriggeringElement()['#index'];
    foreach ($symlinks as $index => &$symlink) {
      $symlink->is_default = $triggeringIndex === $index;
    }
    $form_state->setRebuild();
  }

  public function removeSymlink(array $form, FormStateInterface $form_state) {
    $symlinks =& $form_state->getStorage()['symlinks'];
    $index = $form_state->getTriggeringElement()['#index'];
    unset($symlinks[$index]);
    $form_state->setRebuild();
  }

  /**
   * Adapted from \Drupal\pathauto\PathautoGenerator::createEntityAlias
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function generatePathauto(array $form, FormStateInterface $form_state) {
    if (\Drupal::moduleHandler()->moduleExists('pathauto')) {
//      /** @var \Drupal\pathauto\PathautoGenerator $pathautoGenerator */
//      $pathautoGenerator = \Drupal::service('pathauto.generator');
//      $pattern = $pathautoGenerator->getPatternByEntity($this->entity);
//      if (!$pattern) {
//        return;
//      }
//      try {
//        $internalPath = $this->entity->toUrl()->getInternalPath();
//      }
//      catch (EntityMalformedException $exception) {
//        return;
//      }
//      catch (UndefinedLinkTemplateException $exception) {
//        return;
//      }
//      catch (\UnexpectedValueException $exception) {
//        return;
//      }
//      $source = '/' . $internalPath;
//      $langcode = $this->entity->language()->getId();
//
//      // Core does not handle aliases with language Not Applicable.
//      if ($langcode == LanguageInterface::LANGCODE_NOT_APPLICABLE) {
//        $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED;
//      }
//      /** @var \Drupal\token\TokenEntityMapperInterface $tokenEntityMapper */
//      $tokenEntityMapper = \Drupal::service('token.entity_mapper');
//      /** @var \Drupal\pathauto\AliasCleanerInterface $aliasCleaner */
//      $aliasCleaner = \Drupal::service('pathauto.alias_cleaner');
//      /** @var \Drupal\token\TokenInterface $token */
//      $token = \Drupal::service('token');
//      /** @var \Drupal\pathauto\AliasUniquifierInterface $aliasUniquifier */
//      $aliasUniquifier = \Drupal::service('pathauto.alias_uniquifier');
//
//      // Build token data.
//      $data = [
//        $tokenEntityMapper->getTokenTypeForEntityType($this->entity->getEntityTypeId()) => $this->entity,
//      ];
//      // Replace any tokens in the pattern.
//      // Uses callback option to clean replacements. No sanitization.
//      // Pass empty BubbleableMetadata object to explicitly ignore cacheablity,
//      // as the result is never rendered.
//      $alias = $token->replace($pattern->getPattern(), $data, [
//        'clear' => TRUE,
//        'callback' => [$aliasCleaner, 'cleanTokenValues'],
//        'langcode' => $langcode,
//        'pathauto' => TRUE,
//      ], new BubbleableMetadata());
//
//      // Check if the token replacement has not actually replaced any values. If
//      // that is the case, then stop because we should not generate an alias.
//      // @see token_scan()
//      $pattern_tokens_removed = preg_replace('/\[[^\s\]:]*:[^\s\]]*\]/', '', $pattern->getPattern());
//      if ($alias === $pattern_tokens_removed) {
//        return;
//      }
//
//      $alias = $aliasCleaner->cleanAlias($alias);
//
//      // If we have arrived at an empty string, discontinue.
//      if (!mb_strlen($alias)) {
//        return;
//      }
//
//      // If the alias already exists, generate a new, hopefully unique, variant.
//      $aliasUniquifier->uniquify($alias, $source, $langcode);
//
      $symlinks =& $form_state->getStorage()['symlinks'];
      $index = $form_state->getTriggeringElement()['#index'];
      $input =& $form_state->getUserInput();
      $input['symlinks'][$index]['symlink']['path'] = $this->generatePathautoAliasForRow($input['symlinks'][$index]);
      $form_state->setRebuild();
    }
  }

  protected function generatePathautoAliasForRow($row) {
    /** @var \Drupal\pathauto\PathautoGenerator $pathautoGenerator */
    $pathautoGenerator = \Drupal::service('pathauto.generator');
    $pattern = $pathautoGenerator->getPatternByEntity($this->entity);
    if (!$pattern) {
      return;
    }
    try {
      $internalPath = $this->entity->toUrl()->getInternalPath();
    }
    catch (EntityMalformedException $exception) {
      return;
    }
    catch (UndefinedLinkTemplateException $exception) {
      return;
    }
    catch (\UnexpectedValueException $exception) {
      return;
    }
    $source = '/' . $internalPath;
    $langcode = $this->entity->language()->getId();
    // Core does not handle aliases with language Not Applicable.
    if ($langcode == LanguageInterface::LANGCODE_NOT_APPLICABLE) {
      $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED;
    }
    /** @var \Drupal\token\TokenEntityMapperInterface $tokenEntityMapper */
    $tokenEntityMapper = \Drupal::service('token.entity_mapper');
    /** @var \Drupal\pathauto\AliasCleanerInterface $aliasCleaner */
    $aliasCleaner = \Drupal::service('pathauto.alias_cleaner');
    /** @var \Drupal\token\TokenInterface $token */
    $token = \Drupal::service('token');
    /** @var \Drupal\pathauto\AliasUniquifierInterface $aliasUniquifier */
    $aliasUniquifier = \Drupal::service('pathauto.alias_uniquifier');
    // Build token data.
    $data = [
      $tokenEntityMapper->getTokenTypeForEntityType($this->entity->getEntityTypeId()) => $this->entity,
      'ctek_symlinks' => $row,
    ];
    // Replace any tokens in the pattern.
    // Uses callback option to clean replacements. No sanitization.
    // Pass empty BubbleableMetadata object to explicitly ignore cacheablity,
    // as the result is never rendered.
    $alias = $token->replace($pattern->getPattern(), $data, [
      'clear' => TRUE,
      'callback' => [$aliasCleaner, 'cleanTokenValues'],
      'langcode' => $langcode,
      'pathauto' => TRUE,
    ], new BubbleableMetadata());
    // Check if the token replacement has not actually replaced any values. If
    // that is the case, then stop because we should not generate an alias.
    // @see token_scan()
    $pattern_tokens_removed = preg_replace('/\[[^\s\]:]*:[^\s\]]*\]/', '', $pattern->getPattern());
    if ($alias === $pattern_tokens_removed) {
      return;
    }
    $alias = $aliasCleaner->cleanAlias($alias);
    // If we have arrived at an empty string, discontinue.
    if (!mb_strlen($alias)) {
      return;
    }
    // If the alias already exists, generate a new, hopefully unique, variant.
    $aliasUniquifier->uniquify($alias, $source, $langcode);
    return $alias;
  }

  public function pathautoCallback(array $form, FormStateInterface $form_state) {
    $index = $form_state->getTriggeringElement()['#index'];
    $element = $form['symlinks'][$index]['symlink']['path'];
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#ctek_symlinks_pathauto_path_wrapper_' . $index, $element));
    return $response;
  }

  public function ajaxTableCallback(array $form, FormStateInterface $form_state) {
    return $form['symlinks'];
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    switch ($form_state->getTriggeringElement()['#name']) {
      case 'submit':
        foreach ($form_state->getValue(['symlinks']) as $index => $row) {
          $existing = $this->symlinksManager->getSymlinkByPath($row['symlink']['path'], $row['symlink']['domain']);
          if ($existing && $existing->source !== '/node/' . $this->entity->id()) {
            $form_state->setError($form['symlinks'][$index]['symlink'], $this->t('This symlink path is already in use.'));
          }
        }
        break;
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    switch ($form_state->getTriggeringElement()['#name']) {
      case 'submit':
        $symlinks = [];
        $source = '/node/' . $this->entity->id();
        foreach ($form_state->getValue(['symlinks']) as $row) {
          if ($row['symlink']['domain'] === 0) {
            $row['symlink']['domain'] = NULL;
          }
          $symlinks[] = [
            'source' => $source,
            'symlink' => $row['symlink'],
            'link' => $row['menu_link'],
            'existing_link' => $row['menu_link']['existing_id'],
          ];
        }
        $this->symlinksManager->updateSymlinksForSource($source, $symlinks);
        Cache::invalidateTags([
          'node:' . $this->entity->id(),
          'node_list',
        ]);
        break;
    }
  }

}
