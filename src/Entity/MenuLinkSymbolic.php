<?php

namespace Drupal\ctek_symlinks\Entity;

use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\ctek_symlinks\Plugin\Menu\MenuLinkSymbolic as PluginMenuLinkSymbolic;

class MenuLinkSymbolic extends MenuLinkContent {

  public function getPluginDefinition() {
    $definition = parent::getPluginDefinition();
    $definition['class'] = PluginMenuLinkSymbolic::class;
    return $definition;
  }

}
