<?php

namespace Drupal\ctek_symlinks\Plugin\Menu;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\menu_link_content\Plugin\Menu\MenuLinkContent;
use Drupal\ctek_symlinks\SymlinksManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MenuLinkSymbolic extends MenuLinkContent {

  public static function create(
    ContainerInterface $container,
    array $configuration,
                       $plugin_id,
                       $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('entity.repository'),
      $container->get('ctek_symlinks.symlinks_manager')
    );
  }

  protected $symlinksManager;

  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    LanguageManagerInterface $languageManager,
    EntityRepositoryInterface $entityRepository,
    SymlinksManagerInterface $symlinksManager
  ) {
    $this->symlinksManager = $symlinksManager;
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entityTypeManager,
      $languageManager,
      $entityRepository
    );
  }

  public function getEntityId() {
    return $this->getEntity()->id();
  }

  public function getUrlObject($title_attribute = TRUE) {
    $url = parent::getUrlObject($title_attribute);
    if ($symlink = $this->symlinksManager->getSymlinkFromMenuLink($this)) {
      $url->setOption('symlink', $symlink);
    }

    return $url;
  }

}
