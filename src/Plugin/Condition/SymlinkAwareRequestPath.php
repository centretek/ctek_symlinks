<?php

namespace Drupal\ctek_symlinks\Plugin\Condition;

use Drupal\system\Plugin\Condition\RequestPath;

class SymlinkAwareRequestPath extends RequestPath {

  public function evaluate() {
    $pages = mb_strtolower($this->configuration['pages']);
    if (!$pages) {
      return TRUE;
    }
    $request = $this->requestStack->getCurrentRequest();
    if ($symlink = $request->get('symlink')) {
      return $this->pathMatcher->matchPath($symlink->symlink, $pages);
    }
    return parent::evaluate();
  }

}
