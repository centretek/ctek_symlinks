<?php

namespace Drupal\ctek_symlinks;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\ctek_symlinks\Plugin\Menu\MenuLinkSymbolic;

class SymlinksManager implements SymlinksManagerInterface {

  protected $connection;

  protected $menuLinkManager;

  protected $menuLinkContentStorage;

  protected $cache;

  protected $cacheKey;

  protected $cacheNeedsWriting = FALSE;

  protected $symlinksByMenuPluginId;

  protected $defaultSymlinksBySource;

  protected $symlinksByPath;

  protected $menuLinks;

  protected $skipInvalidation = FALSE;

  protected $cacheInitialized = FALSE;

  public function __construct(
    Connection $connection,
    EntityTypeManagerInterface $entityTypeManager,
    MenuLinkManagerInterface $menuLinkManager,
    CacheBackendInterface $cache
  ) {
    $this->connection = $connection;
    $this->menuLinkManager = $menuLinkManager;
    $this->menuLinkContentStorage = $entityTypeManager->getStorage('menu_link_content');
    $this->cache = $cache;
  }

  protected function ensureCache() {
    if (!$this->cacheInitialized && $this->cacheKey) {
      if ($cached = $this->cache->get($this->cacheKey)) {
        $data = $cached->data;
        $this->defaultSymlinksBySource = $data['defaultSymlinksBySource'];
        $this->symlinksByMenuPluginId = $data['symlinksByMenuPluginId'];
        $this->symlinksByPath = $data['symlinksByPath'];
      } else {
        $this->cacheNeedsWriting = TRUE;
      }
      $this->cacheInitialized = TRUE;
    }
    return $this->cacheInitialized;
  }

  protected function clearCache($source) {
    if ($this->cacheKey) {
      $this->cache->delete('symlinks:' . $source);
      $this->cache->delete($this->cacheKey);
      $this->defaultSymlinksBySource = [];
      $this->symlinksByMenuPluginId = [];
      $this->symlinksByPath = [];
      $this->cacheInitialized = FALSE;
      $this->ensureCache();
    }
  }

  public function setCacheKey($key) {
    $this->cacheKey = 'symlinks:' . $key;
  }

  public function upsert($source, $symlink, MenuLinkSymbolic $link = NULL, $forceDefault = FALSE) {
    $transaction = $this->connection->startTransaction();
    $existing = $this->getSymlinkByPath($symlink['path'], $symlink['domain'] ?? NULL);
    if ($existing && intval($existing->id) !== intval($symlink['id'])) {
      throw new \Exception('Cannot create two symlinks with the same path.');
    }
    try {
      if ($symlink['id']) {
        $symlinkId = $symlink['id'];
        $this->connection
          ->update('ctek_symlinks')
          ->condition('id', $symlink['id'])
          ->fields([
            'link_id' => $link ? $link->getEntityId() : NULL,
            'source' => $source,
            'symlink' => rtrim($symlink['path'], '/'),
            'domain' => $symlink['domain'] ?? NULL,
          ])
          ->execute();
      } else {
        $symlinkId = $this->connection
          ->insert('ctek_symlinks')
          ->fields([
            'link_id',
            'source',
            'symlink',
            'domain',
          ])
          ->values([
            $link ? $link->getEntityId() : NULL,
            $source,
            $symlink['path'],
            $symlink['domain'] ?? NULL,
          ])
          ->execute();
      }
      if (!$symlinkId) {
        throw new \Exception('Unable to save symlink');
      }
      $query = $this->connection->select('ctek_symlinks_default', 'default_symlinks');
      $query->fields('default_symlinks');
      $query->condition('default_symlinks.source', $source);
      $query->condition('default_symlinks.domain', $symlink['domain'] ?? NULL);
      $defaultSymlink = $query->execute()->fetch();
      if (!$defaultSymlink) {
        $this->connection
          ->insert('ctek_symlinks_default')
          ->fields([
            'symlink_id',
            'source',
            'domain',
          ])
          ->values([
            $symlinkId,
            $source,
            $symlink['domain'] ?? NULL,
          ])
          ->execute();
      } elseif ($forceDefault) {
        $this->connection
          ->update('ctek_symlinks_default')
          ->condition('source', $source)
          ->condition('domain', $symlink['domain'] ?? NULL)
          ->expression('symlink_id', ':symlink', [':symlink' => $symlinkId])
          ->execute();
      }
      if (!$this->skipInvalidation) {
        Cache::invalidateTags(['route_match']);
        $this->clearCache($source);
      }
      return $symlinkId;
    } catch (\Exception $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function deleteAllSymlinksBySource($source, array $exclude = []) {
    $transaction = $this->connection->startTransaction();
    try {
      foreach ($this->getAllSymlinksBySource($source) as $symlink) {
        /**
         * Menu items appear to get deleted already on node delete, but maybe
         * not in some cases?
         */
        if (
          $symlink->link_id
          &&
          (
            count($exclude) === 0
            ||
            !in_array(intval($symlink->id), array_map('intval', $exclude))
          )
        ) {
          if ($menuLink = $this->menuLinkContentStorage->load($symlink->link_id)) {
            $menuLink->delete();
          }
        }
      }

      $delete = $this->connection
        ->delete('ctek_symlinks')
        ->condition('source', $source);
      if (count($exclude) > 0) {
        $delete->condition('id', $exclude, 'NOT IN');
      }
      $delete->execute();
      $delete = $this->connection
        ->delete('ctek_symlinks_default')
        ->condition('source', $source);
      if (count($exclude) > 0) {
        $delete->condition('symlink_id', $exclude, 'NOT IN');
      }
      $delete->execute();
      if (!$this->skipInvalidation) {
        Cache::invalidateTags(['route_match']);
        $this->clearCache($source);
      }
    } catch (\Exception $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function updateSymlinksForSource($source, array $symlinks) {
    $transaction = $this->connection->startTransaction();
    try {
      $this->skipInvalidation = TRUE;
      $exclude = [];
      foreach ($symlinks as $symlink) {
        /** @var \Drupal\ctek_symlinks\Entity\MenuLinkSymbolic $link */
        if (isset($symlink['link']['id'])) {
          $link = $this->menuLinkContentStorage->load($symlink['link']['id']);
        } else {
          $link = $this->menuLinkContentStorage->create();
        }
        $linkPlugin = NULL;
        if ($symlink['link']['menu'] && $link) {
          [, $parent] = explode(':', $symlink['link']['parent'], 2);
          $link->set('menu_name', $symlink['link']['menu']);
          $link->set('link', 'internal:' . $symlink['symlink']['path']);
          $link->set('title', $symlink['link']['title']);
          $link->set('parent', $parent);
          $link->save();
          /** @var MenuLinkSymbolic $linkPlugin */
          $linkPlugin = $this->menuLinkManager->createInstance($link->getPluginId(), [
            'entity' => $link,
          ]);
        }
        $exclude[] = $this->upsert($source, $symlink['symlink'], $linkPlugin, $symlink['link']['is_default']);
      }
      $this->deleteAllSymlinksBySource($source, $exclude);
      Cache::invalidateTags(['route_match', 'symlinks:' . $source]);
      $this->clearCache($source);
    } catch (\Exception $e) {
      $transaction->rollBack();
      throw $e;
    } finally {
      $this->skipInvalidation = FALSE;
    }
  }

  public function getSymlinkByPath($path, $domain) {
    $cacheInitialized = $this->ensureCache();
    $key = $domain . $path;
    if (!$cacheInitialized || !isset($this->symlinksByPath[$key])) {
      $query = $this->connection->select('ctek_symlinks', 'symlinks');
      $query->fields('symlinks');
      $query->condition('symlinks.symlink', $path);
      $query->condition('symlinks.domain', $domain);
      $this->symlinksByPath[$key] = $query->execute()->fetch();
      if ($cacheInitialized) {
        $this->cacheNeedsWriting = TRUE;
      }
    }
    return $this->symlinksByPath[$key];
  }

  public function getAllSymlinksBySource($source) {
    $query = $this->connection->select('ctek_symlinks', 'symlinks');
    $query->fields('symlinks');
    $query->condition('symlinks.source', $source);
    $query->orderBy('symlinks.id');
    $query->leftJoin('ctek_symlinks_default', 'default_symlinks', 'default_symlinks.symlink_id = symlinks.id');
    $query->addExpression('CASE WHEN default_symlinks.symlink_id THEN 1 ELSE 0 END', 'is_default');
    return $query->execute()->fetchAll();
  }

  public function getDefaultSymlinkBySource($source, $domain) {
    $cacheInitialized = $this->ensureCache();
    if (!$cacheInitialized || !isset($this->defaultSymlinksBySource[$source])) {
      $query = $this->connection->select('ctek_symlinks', 'symlinks');
      $query->fields('symlinks');
      $query->condition('symlinks.source', $source);
      $query->condition('symlinks.domain', $domain);
      $query->join('ctek_symlinks_default', 'default_symlinks', 'default_symlinks.symlink_id = symlinks.id');
//      if (\Drupal::moduleHandler()->moduleExists('ctek_domain_context')) {
//        [,,$nid] = explode('/', $source);
//        /** @var \Drupal\ctek_domain_context\DomainContextHelper $domainContextHelper */
//        $domainContextHelper = \Drupal::service('ctek_domain_context.helper');
//        $terms = $domainContextHelper->getActiveTerms();
//        $terms = array_map(function(TermInterface $term){
//          return $term->id();
//        }, $terms);
//
//      }
      $this->defaultSymlinksBySource[$source] = $query->execute()->fetch();
      if ($cacheInitialized) {
        $this->cacheNeedsWriting = TRUE;
      }
    }
    return $this->defaultSymlinksBySource[$source];
  }

  public function getSymlinkFromMenuLink(MenuLinkSymbolic $link) {
    $cacheInitialized = $this->ensureCache();
    $pluginId = $link->getPluginId();
    if (!$cacheInitialized || !isset($this->symlinksByMenuPluginId[$pluginId])) {
      [,$uuid] = explode(':', $pluginId);
      $query = $this->connection->select('ctek_symlinks', 'symlinks');
      $query->fields('symlinks');
      $query->join('menu_link_content', 'link', 'link.id = symlinks.link_id');
      $query->condition('link.uuid', $uuid);
      $this->symlinksByMenuPluginId[$pluginId] = $query->execute()->fetch();
      if ($cacheInitialized) {
        $this->cacheNeedsWriting = TRUE;
      }
    }
    return $this->symlinksByMenuPluginId[$pluginId];
  }

  public function getNodeMenuLinkForPath(NodeInterface $node, $path, $domain) {
    if ($symlink = $this->getSymlinkByPath($path, $domain)) {
      if ($symlink->source === '/node/' . $node->id() && $symlink->link_id) {
        return $this->getMenuLinkFromSymlink($symlink);
      }
    }
    return NULL;
  }

  public function getMenuLinkFromSymlink($symlink) {
    if (!$symlink->link_id) {
      return;
    }
    /** @var \Drupal\ctek_symlinks\Entity\MenuLinkSymbolic $menuLink */
    if ($menuLink = $this->menuLinkContentStorage->load($symlink->link_id)) {
      $menuLinkPlugin = $this->menuLinkManager->createInstance($menuLink->getPluginId());
      return $menuLinkPlugin;
    }
  }

  public function writeCache() {
    if ($this->cacheNeedsWriting && !empty($this->cacheKey)) {
      $data = [
        'defaultSymlinksBySource' => $this->defaultSymlinksBySource,
        'symlinksByMenuPluginId' => $this->symlinksByMenuPluginId,
        'symlinksByPath' => $this->symlinksByPath,
      ];
      $this->cache->set($this->cacheKey, $data);
    }
  }

}
