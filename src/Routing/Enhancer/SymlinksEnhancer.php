<?php

namespace Drupal\ctek_symlinks\Routing\Enhancer;

use Drupal\Core\Routing\EnhancerInterface;
use Drupal\ctek_symlinks\SymlinksManagerInterface;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\HttpFoundation\Request;

class SymlinksEnhancer implements EnhancerInterface {

  protected $symlinksManager;

  public function __construct(SymlinksManagerInterface $symlinksManager) {
    $this->symlinksManager = $symlinksManager;
  }

  public function enhance(array $defaults, Request $request) {
    $domain = NULL;
    if (\Drupal::moduleHandler()->moduleExists('ctek_domain_context')) {
      $term = \Drupal::service('ctek_domain_context.helper')->getActiveDomainTerm();
      if ($term instanceof TermInterface) {
        $domain = $term->id();
      }
    }
    if ($symlink = $this->symlinksManager->getSymlinkByPath($request->getPathInfo(), $domain)) {
      $symlink = (array)$symlink;
      $defaults['_raw_variables']->set('symlink', $symlink);
      $defaults['symlink'] = $symlink;
    }
    return $defaults;
  }

}
